//
//  SwiftUIView.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI

/// This is the single page indicator. Page selection and deselection are animated. Managed by the ``PageModel``.
struct PageView: View {
    /// The ModelData fir this view.
    @ObservedObject var page: PageModel
    let animationDuration = 0.5

    var body: some View {
        switch page.animation {
        case .noAnimation(backgroundColor: let color):
            plainSegment(with: color)
        case .cover(backgroundColor: let backgroundColor, selectionColor: let selectionColor):
            segmentWithSelectBehaviour(backgroundColor: backgroundColor, selectionColor: selectionColor)
        case .uncover(backgroundColor: let backgroundColor, selectionColor: let selectionColor):
            segmentWithDeselectBehaviour(backgroundColor: backgroundColor, selectionColor: selectionColor)
        }
    }

    private func plainSegment(with color: Color) -> some View {
        return Rectangle()
            .fill(color)
    }

    private func segmentWithSelectBehaviour(backgroundColor: Color, selectionColor: Color) -> some View {
        return ZStack {
            Rectangle()
                .fill(backgroundColor)
            CoverShape(animationProgress: self.page.animationProgress, animationType: .cover)
                .fill(selectionColor)
                .onAppear {
                    withAnimation(.easeIn(duration: animationDuration)) {
                        self.page.animationProgress = 1.0
                    }
                }
        }
    }

    private func segmentWithDeselectBehaviour(backgroundColor: Color, selectionColor: Color) -> some View {
        return ZStack {
            Rectangle()
                .fill(backgroundColor)
            CoverShape(animationProgress: self.page.animationProgress, animationType: .uncover)
                .fill(selectionColor)
                .onAppear {
                    withAnimation(.easeIn(duration: animationDuration)) {
                        self.page.animationProgress = 1.0
                    }
                }
        }
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(page: PageModel(isSelected: true, animation: .uncover(backgroundColor: .gray, selectionColor: .green)))
    }
}
