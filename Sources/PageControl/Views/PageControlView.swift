//
//  PageControlView.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI


/// Contains pages indicators. Managed by the ``PageControlModel``.
public struct PageControlView: View {
    /// The ModelData fir this view.
    @EnvironmentObject var pageControlModel: PageControlModel

    public init() {}

    public var body: some View {
        return HStack(alignment: .center, spacing: 3.0) {
            ForEach(0..<pageControlModel.pages.count) { pageIndex in
                PageView(page: pageControlModel.pages[pageIndex])
            }
        }
    }
}

struct PageControlView_Previews: PreviewProvider {
    static var backgroundColor: Color = .gray
    static var selectedColor: Color = .green

    static var previews: some View {
        PageControlView()
            .environmentObject(PageControlModel(pagesCount: 5, backgroundColor: .gray, selectionColor: .green))
    }
}
