//
//  File.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI

/// Three types of animation when `PageView` appears
enum PageAnimation {
    /// No animation - just fill the view's background with backgroundColor
    case noAnimation(backgroundColor: Color)
    /// Animation from left to right.
    case cover(backgroundColor: Color, selectionColor: Color)
    /// Animation from right to left.
    case uncover(backgroundColor: Color, selectionColor: Color)
}
