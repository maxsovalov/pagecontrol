//
//  File.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI

/// The shape that can be animated. The target of animation - shape's width
struct CoverShape: Shape {

    /// The animation types supported by shape.
    public enum Animation {
        case cover
        case uncover
    }

    var animationProgress: CGFloat
    var animationType: Animation

    var animatableData: CGFloat {
        get {
            return animationProgress
        }
        set {
            animationProgress = newValue
        }
    }

    func path(in rect: CGRect) -> Path {
        switch animationType {
        case .cover:
            return cover(rect: rect)
        case .uncover:
            return uncover(rect: rect)
        }
    }

    func cover(rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width * animationProgress, y: 0))
        path.addLine(to: CGPoint(x: rect.width * animationProgress, y: rect.height))
        path.addLine(to: CGPoint(x: 0, y: rect.height))
        path.closeSubpath()
        return path
    }

    func uncover(rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width - (rect.width * animationProgress), y: 0))
        path.addLine(to: CGPoint(x: rect.width - (rect.width * animationProgress), y: rect.height))
        path.addLine(to: CGPoint(x: 0, y: rect.height))
        path.closeSubpath()
        return path
    }
}
