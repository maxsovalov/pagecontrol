//
//  File.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI

/// This is the DataModel for PageControlView (SwiftUI view).
public final class PageControlModel: ObservableObject {
    /// The array of pages. This array is created when you create instance of current class.
    @Published var pages: [PageModel]
    /// The color of the page indicator that means that page is not selected.
    let backgroundColor: Color
    /// The color of the page indicator that means that page is selected. Also used for animation - cover or uncover the page's indicator background when selection state changes.
    let selectionColor: Color

    /// Creates new instance of current class and fills `pages` array based on ``pagesCount`` parameter.
    /// - Parameters:
    ///   - pagesCount: Total count of pages.
    ///   - backgroundColor: The color of the page indicator that means that page is not selected.
    ///   - selectionColor: The color of the page indicator that means that page is selected. Also used for animation - cover or uncover the page's indicator background when selection state changes.
    public init(pagesCount: Int, backgroundColor: Color, selectionColor: Color) {
        self.backgroundColor = backgroundColor
        self.selectionColor = selectionColor
        pages = []
        for _ in (0..<pagesCount) {
            pages.append(PageModel(isSelected: false, animation: .noAnimation(backgroundColor: backgroundColor)))
        }
    }

    /// Selects the next unselected page if one exists.
    public func nextPage() {
        // look for first unselected page
        guard let firstUnselectedIndex = pages.firstIndex(where: { !$0.isSelected }) else {
            // return if no such page - all pages are selected
            print("no unselected pages available")
            return
        }
        // we have to change previous page's animation
        if firstUnselectedIndex != pages.startIndex {
            let previousPageIndex = pages.index(before: firstUnselectedIndex)
            pages[previousPageIndex] = PageModel(isSelected: true, animation: .noAnimation(backgroundColor: selectionColor))
        }
        // and now change the isSelected and animation in page that is selected
        pages[firstUnselectedIndex] = PageModel(isSelected: true, animation: .cover(backgroundColor: backgroundColor, selectionColor: selectionColor))
    }

    /// Deselects the last selected page if one exists.
    public func previousPage() {
        // look for last selected page
        guard let lastSelectedIndex = pages.lastIndex(where: { $0.isSelected }) else {
            // return if no such page - all pages are selected
            print("no selected pages available")
            return
        }
        // and now change the isSelected and animation in page that is deselected
        pages[lastSelectedIndex] = PageModel(isSelected: false, animation: .uncover(backgroundColor: backgroundColor, selectionColor: selectionColor))
    }
}
