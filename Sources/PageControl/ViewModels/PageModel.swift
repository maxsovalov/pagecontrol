//
//  File.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-27.
//

import SwiftUI

/// This is the DataModel for PageView (SwiftUI view).
final class PageModel: ObservableObject {
    /// The page's selection state.
    let isSelected: Bool
    /// The type of appear animation of page
    let animation: PageAnimation
    /// This variable used by SwiftUI animation inside the view. Initial value equals 0.
    @Published var animationProgress: CGFloat = 0.0


    /// Creates new instance of current class.
    /// - Parameters:
    ///   - isSelected: The page's selection state.
    ///   - animation: The type of appear animation of page.
    init(isSelected: Bool, animation: PageAnimation) {
        self.isSelected = isSelected
        self.animation = animation
    }
}
