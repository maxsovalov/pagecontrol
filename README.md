# PageControl

This Package contains `PageControl` SwiftUI view.
The `PageControl` view displays the current position of the user when navigating through a multi-page process.
The `PageControl` view displays the set of page indicator which are `selected` or `deselected`.
To manage view you use the `PageControlModel` instance.
Use model's methods `nextPage()` and `previousPage()` to select or deselect pages in view.
