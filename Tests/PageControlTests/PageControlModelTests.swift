//
//  PageControlModelTests.swift
//  
//
//  Created by Maksym Sovalov on 2021-10-28.
//

import XCTest
import SwiftUI
@testable import PageControl

final class PageControlModelTests: XCTestCase {
    func testPagesCountOnInit() throws {
        // PRECINDITION
        let pagesCount = 5

        // INVOCATION
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: .gray, selectionColor: .green)

        // ASSERTION
        XCTAssertEqual(pagesCount, sut.pages.count)
    }

    func testPagesStateOnInit() {
        // PRECINDITION
        let pagesCount = 5

        // INVOCATION
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: .gray, selectionColor: .green)

        // ASSERTION
        for page in sut.pages {
            XCTAssertFalse(page.isSelected)
        }
    }

    func testPagesAnimationProgressInit() {
        // PRECINDITION
        let pagesCount = 5

        // INVOCATION
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: .gray, selectionColor: .green)

        // ASSERTION
        for page in sut.pages {
            XCTAssertEqual(0, page.animationProgress)
        }
    }

    func testPagesAnimationInit() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray

        // INVOCATION
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: .green)

        // ASSERTION
        for page in sut.pages {
            switch page.animation {
            case .noAnimation(let color):
                XCTAssertEqual(backgroundColor, color)
            case .cover(_, _):
                XCTAssertTrue(false)
            case .uncover(_, _):
                XCTAssertTrue(false)
            }
        }
    }

    func testPagesStateAfterNext() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: .green)

        // INVOCATION
        sut.nextPage()

        // ASSERTION
        for index in 0..<sut.pages.count {
            let page = sut.pages[index]
            if index == 0 {
                XCTAssertTrue(page.isSelected)
            } else {
                XCTAssertFalse(page.isSelected)
            }
        }
    }

    func testPagesAnimationAfterNext() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray
        let selectionColor = Color.green
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: selectionColor)

        // INVOCATION
        sut.nextPage()

        // ASSERTION
        for index in 0..<sut.pages.count {
            let page = sut.pages[index]
            switch page.animation {
            case .noAnimation(_):
                if index == 0 {
                    XCTAssertTrue(false)
                }
            case .cover(let color1, let color2):
                if index == 0 {
                    XCTAssertEqual(backgroundColor, color1)
                    XCTAssertEqual(selectionColor, color2)
                } else {
                    XCTAssertTrue(false)
                }
            case .uncover(_, _):
                XCTAssertTrue(false)
            }
        }
    }

    func testPagesStateWhenAllSelected() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: .green)

        // INVOCATION
        for _ in 1...pagesCount {
            sut.nextPage()
        }

        // ASSERTION
        for page in sut.pages {
            XCTAssertTrue(page.isSelected)
        }
    }

    func testPagesStateAfterPrevious() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: .green)

        // INVOCATION
        sut.nextPage()
        sut.nextPage()
        sut.previousPage()

        // ASSERTION
        for index in 0..<sut.pages.count {
            let page = sut.pages[index]
            if index == 0 {
                XCTAssertTrue(page.isSelected)
            } else {
                XCTAssertFalse(page.isSelected)
            }
        }
    }

    func testPagesAnimationAfterPrevious() {
        // PRECINDITION
        let pagesCount = 5
        let backgroundColor = Color.gray
        let selectionColor = Color.green
        let sut = PageControlModel(pagesCount: pagesCount, backgroundColor: backgroundColor, selectionColor: selectionColor)

        // INVOCATION
        sut.nextPage()
        sut.nextPage()
        sut.previousPage()

        // ASSERTION
        for index in 0..<sut.pages.count {
            let page = sut.pages[index]
            switch page.animation {
            case .noAnimation(_):
                if index == 1 {
                    XCTAssertTrue(false, "index = \(index)")
                }
            case .cover(_, _):
                XCTAssertTrue(false, "index = \(index)")
            case .uncover(let color1, let color2):
                if index == 1 {
                    XCTAssertEqual(backgroundColor, color1, "index = \(index)")
                    XCTAssertEqual(selectionColor, color2, "index = \(index)")
                } else {
                    XCTAssertTrue(false, "index = \(index)")
                }
            }
        }
    }
}
